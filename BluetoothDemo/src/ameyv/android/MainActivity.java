package ameyv.android;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

	
	private static String address = "00:06:66:6C:61:51";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private BluetoothSocket btSocket = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private OutputStream outStream = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mBluetoothAdapter.cancelDiscovery();
		
	}
	
	public void connect(View view) {
		try {
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
			btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
			btSocket.connect();
		} catch (IOException e) {

		}
	}
	
	public void sendData(View view) {
		writeData("444");
	}
	
	private void writeData(String data) {
		try {
			outStream = btSocket.getOutputStream();
		} catch (IOException e) {

		}
		String message = data;
		byte[] msgBuffer = message.getBytes();
		try {
			outStream.write(msgBuffer);
		} catch (IOException e) {

		}
	}

}
