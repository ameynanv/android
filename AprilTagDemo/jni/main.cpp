// JNI dependencies
#include <jni.h>

// Android dependencies
#include <android/log.h>
#include <android_native_app_glue.h>

// Opencv dependencies
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <string>
// AprilTag dependencies
#include "apriltag.h"
#include "common/image_u8.h"
#include "tag36h11.h"
#include "common/zarray.h"

using namespace std;
using namespace cv;

// Logging on native part
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "apriltag-native-lib-tag", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "apriltag-native-lib-tag", __VA_ARGS__))

// Global variable references
apriltag_family_t *tf;
apriltag_detector_t *td;
Mat intrinsicMatrix;
Mat distortionMatrix;

extern "C" {


	image_u8_t *image_u8_create_from_image(Mat& mRgb)
	{
		image_u8_t *im = NULL;
		im = image_u8_create(mRgb.cols, mRgb.rows);
		uchar *input = (uchar*)(mRgb.data);
		// Gray conversion for RGB is gray = (r + g + g + b)/4
		int x, y;
		for (y = 0; y < im->height; y++) {
			for (x = 0; x < im->width; x++) {
				im->buf[y*im->stride + x] = input[mRgb.step * y + x ];
			}
		}
		return im;
	}

	JNIEXPORT jfloat JNICALL Java_ameyv_android_MainActivity_FindAprilTag(JNIEnv*, jobject, jlong addrDetector, jlong addrRgba, jlong addrGray);
	JNIEXPORT jfloat JNICALL Java_ameyv_android_MainActivity_FindAprilTag(JNIEnv*, jobject, jlong addrDetector, jlong addrRgba, jlong addrGray) {
		LOGI("Inside FindAprilTag");
		apriltag_detector_t *detector = (apriltag_detector_t *)addrDetector;
		Mat& mRgb = *(Mat*)addrRgba;
		Mat& gray = *(Mat*)addrGray;
		float yawAngle;
		image_u8_t *im = image_u8_create_from_image(gray);
		zarray_t *detections = apriltag_detector_detect(detector, im);
		for (int i = 0; i < zarray_size(detections); i++) {
			vector<Point2f> imagePoints;
			vector<Point3f> tagPoints;
			apriltag_detection_t *det;
			zarray_get(detections, i, &det);
			line(mRgb, cvPoint(det->p[0][0], det->p[0][1]), cvPoint(det->p[1][0], det->p[1][1]), cvScalar(255,0,0,0), 4,8,0);
			line(mRgb, cvPoint(det->p[1][0], det->p[1][1]), cvPoint(det->p[2][0], det->p[2][1]), cvScalar(0,255,0,0), 4,8,0);
			line(mRgb, cvPoint(det->p[2][0], det->p[2][1]), cvPoint(det->p[3][0], det->p[3][1]), cvScalar(255,255,0,0), 4,8,0);
			line(mRgb, cvPoint(det->p[3][0], det->p[3][1]), cvPoint(det->p[0][0], det->p[0][1]), cvScalar(0,0,255,0), 4,8,0);
			line(mRgb, cvPoint(det->p[1][0], det->p[1][1]), cvPoint(det->p[3][0], det->p[3][1]), cvScalar(0,255,0,0), 4,8,0);
			line(mRgb, cvPoint(det->p[2][0], det->p[2][1]), cvPoint(det->p[0][0], det->p[0][1]), cvScalar(255,255,0,0), 4,8,0);

			imagePoints.push_back(Point2f(det->p[0][0], det->p[0][1]));
			imagePoints.push_back(Point2f(det->p[1][0], det->p[1][1]));
			imagePoints.push_back(Point2f(det->p[2][0], det->p[2][1]));
			imagePoints.push_back(Point2f(det->p[3][0], det->p[3][1]));

			tagPoints.push_back(Point3f(-0.07, 	0.07, 0));
			tagPoints.push_back(Point3f(0.07,	0.07, 0));
			tagPoints.push_back(Point3f(0.07,	-0.07, 0));
			tagPoints.push_back(Point3f(-0.07,	-0.07, 0));

			Mat rvecTag0;
			Mat tvecTag0;
			Mat rotationMatrix;

			solvePnPRansac(tagPoints, imagePoints, intrinsicMatrix, distortionMatrix, rvecTag0, tvecTag0, false, CV_P3P);
			Rodrigues(rvecTag0, rotationMatrix);


			yawAngle = 180.00 / M_PI * atan2(-rotationMatrix.at<double>(2,0), sqrt(rotationMatrix.at<double>(2,1)*rotationMatrix.at<double>(2,1) + rotationMatrix.at<double>(2,2) * rotationMatrix.at<double>(2,2)));
			//__android_log_print(ANDROID_LOG_INFO, "TRANSLATION", "[ %.3f %.3f %.3f]", tvecTag0.at<double>(0,0), tvecTag0.at<double>(1,0), tvecTag0.at<double>(2,0));
			//__android_log_print(ANDROID_LOG_INFO, "ROTATION", "[%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\n]",rotationMatrix.at<double>(0,0),rotationMatrix.at<double>(0,1),rotationMatrix.at<double>(0,2),rotationMatrix.at<double>(1,0),rotationMatrix.at<double>(1,1),rotationMatrix.at<double>(1,2),rotationMatrix.at<double>(2,0),rotationMatrix.at<double>(2,1),rotationMatrix.at<double>(2,2));
			//__android_log_print(ANDROID_LOG_INFO, "ROTATION", "[%.3f]", yawAngle * 180 / M_PI);
			imagePoints.clear();
			tagPoints.clear();

			apriltag_detection_destroy(det);
		}
		zarray_destroy(detections);
		image_u8_destroy(im);
		return (jfloat)yawAngle;
	}

	JNIEXPORT jlong JNICALL Java_ameyv_android_MainActivity_InitializeAprilTagDetector(JNIEnv*, jobject);
	JNIEXPORT jlong JNICALL Java_ameyv_android_MainActivity_InitializeAprilTagDetector(JNIEnv*, jobject) {
		LOGI("Inside InitializeAprilTagDetector");
		tf = tag36h11_create();
		td = apriltag_detector_create();
		apriltag_detector_add_family(td, tf);
		td->quad_decimate = 0.0;
		td->quad_sigma = 0.0;
		td->nthreads = 4;
		td->debug = 0;
		td->refine_decode = 0;
		td->refine_pose = 0;
		intrinsicMatrix =(Mat_<float>(3,3) << 821.542, 0.000, 319.500, 0.000, 821.542, 239.500,	0.000, 0.000, 1.000);
		distortionMatrix =(Mat_<float>(5,1) << -0.423, 10.972, 0.000, 0.000, -61.434);
		return (jlong)td;
	}

	JNIEXPORT void JNICALL Java_ameyv_android_MainActivity_DestroyAprilTagDetector(JNIEnv*, jobject);
	JNIEXPORT void JNICALL Java_ameyv_android_MainActivity_DestroyAprilTagDetector(JNIEnv*, jobject) {
		LOGI("Inside DestroyAprilTagDetector");
		apriltag_detector_destroy(td);
		tag36h11_destroy(tf);
	}

}
