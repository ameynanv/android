LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=STATIC
include /home/amey/sdks/OpenCV-2.4.10-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := apriltag-native-lib
LOCAL_SRC_FILES := main.cpp common/image_u8.c common/pnm.c tag36h11.c apriltag.c common/zarray.c common/time_util.c common/matd.c common/svd22.c common/workerpool.c common/homography.c apriltag_quad_thresh.c common/image_u32.c g2d.c common/zhash.c common/zmaxheap.c common/unionfind.c
LOCAL_LDLIBS    += -llog -landroid -ldl
LOCAL_STATIC_LIBRARIES += android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
