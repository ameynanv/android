package ameyv.android;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;


public class MainActivity extends Activity implements CvCameraViewListener2  {

    public native float FindAprilTag(long addrDetector, long matAddrRgba, long matAddrGray);
    public native long InitializeAprilTagDetector();
    public native void DestroyAprilTagDetector();
    
    private CameraBridgeViewBase mOpenCvCameraView;
    
    private Mat mColor;
    private Mat mGray;
    private long aprilTagDetector;
    private int mResolutionWidth = 640;
    private int mResolutionHeight = 480;
    
    
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("apriltag-native-lib");
                    mOpenCvCameraView.enableView();
                    aprilTagDetector = InitializeAprilTagDetector();
                    
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_main_camera_view);
        mOpenCvCameraView.setMaxFrameSize(mResolutionWidth, mResolutionHeight);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings1) {
            Toast.makeText(getApplicationContext(), "1280 x 768", Toast.LENGTH_LONG).show();
            return true;
        }
        if (id == R.id.action_settings2) {
            mResolutionWidth = 640;
            mResolutionHeight = 480;
            mOpenCvCameraView.setMaxFrameSize(mResolutionWidth, mResolutionHeight);
            Toast.makeText(getApplicationContext(), "640 x 480", Toast.LENGTH_LONG).show();
            return true;
        }
        if (id == R.id.action_settings3) {
            Toast.makeText(getApplicationContext(), "320 x 240", Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        
    }

    @Override
    public void onCameraViewStopped() {
        DestroyAprilTagDetector();
    }

    @Override
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mGray = inputFrame.gray();
        mColor = inputFrame.rgba();
        float angle = 0;
        angle = FindAprilTag(aprilTagDetector, mColor.getNativeObjAddr(), mGray.getNativeObjAddr());
        Log.i("JAVA_APRIL_TAG", "" + angle);
        return mColor;
    }
    

}