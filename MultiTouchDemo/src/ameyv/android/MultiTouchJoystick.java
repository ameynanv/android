package ameyv.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

public class MultiTouchJoystick extends View {

	private Integer canvasHeight;
	private Integer canvasWidth;
	private Paint controllerBodyStroke;
	private Paint controllerBodyFill;
	private PointF leftControllerCenter;
	private PointF rightControllerCenter;
	private Float controllerRadius;
	
	private PointF leftStickPosition;
	private PointF rightStickPosition;
	
	private SparseArray<PointF> mActivePointers;
	private Paint textPaint;
	
	private int throttle;
	private int rudder;
	private int ailron;
	private int elevator;
	private int pThrottle;
	private int pRudder;
	private int pAilron;
	private int pElevator;

	

	
	public MultiTouchJoystick(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}
	
	private void getValuesFromStickPositions() {
		if (controllerRadius != null && leftControllerCenter != null && 
			rightControllerCenter != null && leftStickPosition != null &&
			rightStickPosition != null) 
		{
			
		}
	}
	
	private void initView() {
//		mActivePointers = new SparseArray<PointF>();
//		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//		// set painter color to a color you like
//		mPaint.setColor(Color.BLUE);
//		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setTextSize(20);
		
		mActivePointers = new SparseArray<PointF>();
		controllerBodyStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
		controllerBodyStroke.setStyle(Paint.Style.STROKE);
		controllerBodyStroke.setColor(Color.BLACK);
		controllerBodyStroke.setStrokeWidth(5.0f);
		
		controllerBodyFill = new Paint(Paint.ANTI_ALIAS_FLAG);
		controllerBodyFill.setStyle(Paint.Style.FILL_AND_STROKE);
		controllerBodyFill.setColor(Color.BLACK);
		
		throttle = -100;
		rudder = 0;
		ailron = 0;
		elevator = 0;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// get pointer index from the event object
		int pointerIndex = event.getActionIndex();

		// get pointer ID
		int pointerId = event.getPointerId(pointerIndex);

		// get masked (not specific to a pointer) action
		int maskedAction = event.getActionMasked();

		PointF f = new PointF();
		f.x = event.getX(pointerIndex);
		f.y = event.getY(pointerIndex);
		
		switch (maskedAction) {

		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN: {
			// We have a new pointer. Lets add it to the list of pointers
			if (f.x <= canvasWidth/2) {
				leftStickPosition = f;
				pThrottle = throttle;
				pRudder = rudder;
				
			} else {
				rightStickPosition = f;
				pAilron = ailron;
				pElevator = elevator;
			}
			mActivePointers.put(pointerId, f);
			break;
		}
		case MotionEvent.ACTION_MOVE: { // a pointer was moved
			for (int size = event.getPointerCount(), i = 0; i < size; i++) {
				PointF point = mActivePointers.get(event.getPointerId(i));
				if (point.x <= canvasWidth / 2) {
					rudder = pRudder + (int)((((event.getX(i) - leftStickPosition.x)*1.0)/controllerRadius)*100); 
				    if (rudder > 100) rudder = 100;
				    if (rudder < -100) rudder = -100;
				    throttle = pThrottle + (int)((((leftStickPosition.y - event.getY(i))*1.0)/controllerRadius)*100);
				    if (throttle > 100) throttle = 100;
				    if (throttle < -100) throttle = -100;
				} else {
					ailron = pAilron + (int)((((event.getX(i) - rightStickPosition.x)*1.0)/controllerRadius)*100);
				    if (ailron > 100) ailron = 100;
				    if (ailron < -100) ailron = -100;
				    elevator = pElevator + (int)((((rightStickPosition.y - event.getY(i))*1.0)/controllerRadius)*100);
				    if (elevator > 100) elevator = 100;
				    if (elevator < -100) elevator = -100;
				}
			}
			break;
		}
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
		case MotionEvent.ACTION_CANCEL: {
			mActivePointers.remove(pointerId);
			break;
		}
		}
		invalidate();

		return true;
	}

	/*
	 * This method draws two Circles on the screen
	 */
	private void drawControllers(Canvas canvas) {
		// Left Foreground Circle		
		canvas.drawCircle(leftControllerCenter.x, leftControllerCenter.y, controllerRadius, controllerBodyStroke);
		// Right Foreground Circle		
		canvas.drawCircle(rightControllerCenter.x, rightControllerCenter.y, controllerRadius, controllerBodyStroke);
	}

	/*
	 * This method draws two Circles on the screen
	 */
	private void drawSticksOnController(Canvas canvas, int throttle, int rudder, int elevator, int ailron) {
		
		float leftStickY = (float)(leftControllerCenter.y - throttle * controllerRadius / 100.0);
		float leftStickX = (float)(rudder * controllerRadius / 100.0 + leftControllerCenter.x);
		PointF leftStick = new PointF(leftStickX, leftStickY);
		canvas.drawCircle(leftStick.x, leftStick.y, 40, controllerBodyFill);
		canvas.drawLine(leftStick.x, leftStick.y, leftControllerCenter.x, leftControllerCenter.y, controllerBodyStroke);
		
		float rightStickY = (float)(rightControllerCenter.y - elevator * controllerRadius / 100.0);
		float rightStickX = (float)(ailron * controllerRadius / 100.0 + rightControllerCenter.x);
		PointF rightStick = new PointF(rightStickX, rightStickY);
		canvas.drawCircle(rightStick.x, rightStick.y, 40, controllerBodyFill);
		canvas.drawLine(rightStick.x, rightStick.y, rightControllerCenter.x, rightControllerCenter.y, controllerBodyStroke);
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (leftControllerCenter == null || rightControllerCenter == null) {
			canvasHeight = Integer.valueOf(canvas.getHeight());
			canvasWidth =  Integer.valueOf(canvas.getWidth());
			leftControllerCenter = new PointF((float)(1.0*canvasWidth/4.0), (float)(1.0*canvasHeight/2.0));
			rightControllerCenter = new PointF((float)(3.0*canvasWidth/4.0), (float)(1.0*canvasHeight/2.0));
		}
		if (controllerRadius == null) {
			controllerRadius = Float.valueOf((float)(0.92*canvas.getWidth()/4.0));
		}
		drawControllers(canvas);
		drawSticksOnController(canvas,throttle, rudder, elevator, ailron);
		canvas.drawText("T=" + throttle + " R=" + rudder + " E=" + elevator + " A=" + ailron, 10, 40 , textPaint);
	}

}
